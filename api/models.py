from django.db import models

# Create your models here.
# Post 
# user_id
# post_id
# likes_count

class Post(models.Model):
    user_id = models.CharField(unique=False, max_length=20)
    likes_count = models.CharField(max_length=9000000000)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['created']



