from django.test import TestCase
from .models import Post

# Create your tests here.


class PostTestCase(TestCase):
    def setUp(self):
        post_a = Post(user_id='1', likes_count='10')
        post_a.save()
        post_b = Post(user_id='1', likes_count='20')
        post_b.save()
        post_c = Post(user_id='2', likes_count='30')
        post_c.save()
        post_d = Post(user_id='3', likes_count='40')
        post_d.save()
        post_e = Post(user_id='1', likes_count='50')
        post_e.save()
        post_f = Post(user_id='2', likes_count='60')
        post_f.save()

    def test_post_count(self):
        post_count = Post.objects.all().count()
        self.assertEqual(post_count, len(Post.objects.all()) )


    def test_post_sorted(self):
        posts = Post.objects.all()
        last_post = Post.objects.get(pk=6)
        # Because of zero indexing, minus 1 from the length
        self.assertEqual(last_post, posts[len(Post.objects.all()) - 1])