from django.urls import path, include
from rest_framework import routers  
from api import views

from django.contrib import admin


router = routers.DefaultRouter()
router.register(r'posts', views.PostViewset)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    path('scraper/', include('scrapper.urls')),

]
