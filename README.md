# Scraper
## The Growth-X Data Extractor
##
[![N|Solid](https://d0.awsstatic.com/logos/powered-by-aws.png)](https://#)
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)


## Useful paths
```
'' # Api Home 
'posts' # Here we can create our Posts to act as Social Media Api Json Response
'scraper/' # Scraper Landing Page to run statistics
```


## Install app -Linux
```
python3 -m venv .env
source .env/bin/activate
pip install -r requirements.py

git clone

```
