from django.shortcuts import render
from rest_framework.decorators import api_view
from api.serializers import PostSerializer
from api.models import Post
from django.http import JsonResponse, HttpResponse
from rest_framework.response import Response
from .forms import PostIdForm, UserIdForm
import pickle
from datetime import datetime
# Create your views here.
def index(request):
    """
    Return Basic Home Page
    """
    return render(request, 'v1/website.html')

def save_(name, data):
    _now = str(datetime.now())
    with open(f'{name}_{_now}.data', 'wb') as f:
        pickle.dump(data, f)

@api_view(['GET'])
def all_statistics(request):
    """
    List all posts
    """
    if request.method == 'GET':
        data = list()
        posts = Post.objects.all()
        serializer = PostSerializer(posts, many=True, context={'request': request})
        for x in (serializer.data[:11]):
            data.append(x)
        all_stat = {'all_stat': data}
        save_('all_stat', data)
        return JsonResponse(all_stat, safe=False)

@api_view(['GET'])
def latest_statistics(request):
    """
    List posts:  top 10
    """
    if request.method == 'GET':
        data = list()
        posts = Post.objects.all()
        serializer = PostSerializer(posts, many=True, context={'request': request})
        for x in (serializer.data[:11]):  # Get the latest 10 posts
            data.append(x)
        latest_10 = {'latest_10' : data}

        save_('latest_10', latest_10)


        return JsonResponse(latest_10, safe=False)

def latest(request):
    """
    List latest post
    """
    if request.method == 'GET':
        posts = Post.objects.last()
        serializer = PostSerializer(posts, context={'request': request})
        latest = serializer.data
        save_('first', latest)

        return JsonResponse(latest, safe=False)



def statistics_by_post_id(request):
    """
    Retrieve a post.
    """
    
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = PostIdForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            your_id = form.cleaned_data['your_id']
            try:
                post = Post.objects.get(pk=your_id)
            except Post.DoesNotExist:
                return HttpResponse(status=404)

            serializer = PostSerializer(post, context={'request': request})

            save_('by_post_id', serializer.data)


            return JsonResponse(serializer.data)

    # if a GET (or any other method) we'll create a blank form
    else:
        form = PostIdForm()

    return render(request, 'v1/by_id.html', {'form': form})

def statistics_by_user_id(request):
    """
    TODO : Retrieve a post by user Id

    """
    
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = UserIdForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            your_id = form.cleaned_data['your_id']
            print(your_id)
            try:
                posts = Post.objects.filter(user_id=your_id)
            except Post.DoesNotExist:
                return HttpResponse(status=404)

            serializer = PostSerializer(posts, context={'request': request})

            # save_('by_user_id', serializer.data) # Not working yet

            return JsonResponse(serializer.data)

    # if a GET (or any other method) we'll create a blank form
    else:
        form = UserIdForm()

    return render(request, 'v1/by_user_id.html', {'form': form})
