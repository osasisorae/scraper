from django.test import TestCase
from api.models import Post

# Create your tests here.

class ScrapperTestCase(TestCase):
    def setUp(self):
        post_a = Post(user_id='1', likes_count='10')
        post_a.save()
        post_b = Post(user_id='1', likes_count='20')
        post_b.save()
        post_c = Post(user_id='2', likes_count='30')
        post_c.save()
        post_d = Post(user_id='3', likes_count='40')
        post_d.save()
        post_e = Post(user_id='1', likes_count='50')
        post_e.save()
        post_f = Post(user_id='2', likes_count='60')
        post_f.save()
        self.post_f = post_f

    def test_latest_post(self):
        latest_post = Post.objects.last()
        self.assertEqual(latest_post, self.post_f)

    def test_latest10_post(self):
        # TODO : Get latest 10
        posts = Post.objects.all()
        # self.assertEqual(latest_post, self.post_f)