from django import forms

class PostIdForm(forms.Form):
    your_id = forms.CharField(label='Your id', max_length=100000000)

class UserIdForm(forms.Form):
    your_id = forms.CharField(label='Your id', max_length=100000000)