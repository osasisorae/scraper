from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index),
    path('all_statistics', views.all_statistics, name='all_statistics'),
    path('latest_statistics', views.latest_statistics, name='latest_statistics'),
    path('latest', views.latest, name='latest'),
    path('post_id_detail', views.statistics_by_post_id, name='post_id'),
    path('user_id_detail', views.statistics_by_user_id, name='user_id'),
]
